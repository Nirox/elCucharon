/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entitiy.Reserve;
import Entitiy.Restaurant;
import Entitiy.RestaurantSession;
import Entitiy.beans.ClientReserver;
import Entitiy.beans.Message;
import Entitiy.beans.ReserveClientRestaurant;
import Entitiy.beans.TablesReserved;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.Utils;

@Stateless
@Path("entitiy.reserve")
public class ReserveFacadeREST extends AbstractFacade<Reserve> {

    @EJB
    private RestaurantFacadeREST restaurantFacadeREST;

    @PersistenceContext(unitName = "RestaurantBookingServerPU")
    private EntityManager em;

    @Inject
    RestaurantSessionFacadeREST restaurantSessionFacadeREST;

    public ReserveFacadeREST() {
        super(Reserve.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Message create(ClientReserver clientReserver) {
        Message msg = new Message();
        Reserve reserve = new Reserve();
        reserve.setEmail(clientReserver.getEmail());
        reserve.setDateReserve(clientReserver.getDateReserve());
        reserve.setTables(clientReserver.getTables());
        reserve.setTelephone(clientReserver.getTelephone());

        RestaurantSession session = restaurantSessionFacadeREST.find(clientReserver.getIdSession());
        reserve.setIdSession(session);
        //send email
        int tables = 0;
        for (Reserve res : session.getReserveList()) {
            if (res.getDateReserve().equals(reserve.getDateReserve()) && res.getIdSession().getId().intValue() == clientReserver.getIdSession().intValue()) {
                tables += res.getTables().intValue();
            }
        }
        if (session.getTables().intValue() - tables - reserve.getTables().intValue() >= 0) {
            super.create(reserve);
            session.getReserveList().add(reserve);
            msg.setMessage("OK");
        } else {
            msg.setMessage("KO");
        }
        return msg;
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") BigDecimal id, Reserve entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") BigDecimal id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Reserve find(@PathParam("id") BigDecimal id) {
        return super.find(id);
    }

    @Override
    public List<Reserve> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Reserve> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @GET
    @Path("tables/{idrestaurant}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<TablesReserved> getTablesReserved(@PathParam("idrestaurant") BigDecimal idRestaurant) {
        List<TablesReserved> list = new ArrayList<>();
        Query query = this.em.createNativeQuery("select r.id_session, sum(r.tables) as tables, TO_CHAR(r.date_reserve, 'dd-MM-YYYY') as dateReserve from reserve r, restaurant_session rs where r.id_session = rs.id and rs.id_restaurant = ?restaurant group by TO_CHAR(r.date_reserve, 'dd-MM-YYYY'), r.ID_SESSION");
        query.setParameter("restaurant", idRestaurant.intValue());
        List<Object[]> tables = query.getResultList();
        for (Object[] table : tables) {
            list.add(new TablesReserved((BigDecimal) table[0], (BigDecimal) table[1], (String) table[2]));
        }
        return list;
    }

    @GET
    @Path("restaurant/{idrestaurant}/{token}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<ReserveClientRestaurant> getReserveByRestaurant(@PathParam("idrestaurant") BigDecimal idRestaurant, @PathParam("token") String token) {

        List<ReserveClientRestaurant> list = new ArrayList<>();
        Restaurant restaurant = this.restaurantFacadeREST.find(idRestaurant);
        String secret = restaurant.getSecretNumber();
        String publicKey = restaurant.getPublicKey();

        /*publicKey = publicKey.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
        try {
            //System.out.println("Texto luego: " + parseado);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(java.util.Base64.getDecoder().decode(publicKey));
            RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

            //Certificate cert = loadCertificate(publicKeyPath);
            Signature s = Signature.getInstance("SHA256withRSA");
            s.initVerify(pubKey);
            //s.initVerify(cert);
            s.update(secret.getBytes());
            //s.update(signedContent);
            //s.verify()
            if (s.verify(java.util.Base64.getDecoder().decode(token))) {
                System.out.println("signature signed by matching key");
            } else {
                System.out.println("signature NOT signed by matching key");
            }

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ReserveFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(ReserveFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(ReserveFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(ReserveFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        Query query = this.em.createNativeQuery("SELECT re.telephone, re.date_reserve, re.tables, re.email, s.open FROM Reserve re, Restaurant_Session s where re.id_session = s.id and s.id_restaurant = ?restaurant order by re.date_reserve, s.open");
        query.setParameter("restaurant", idRestaurant.intValue());
        List<Object[]> reserves = query.getResultList();
        for (Object[] reserve : reserves) {
            list.add(new ReserveClientRestaurant((String) reserve[0], Utils.dateToString((Date) reserve[1]), (BigDecimal) reserve[2], (String) reserve[3], (String) reserve[4]));
        }
        //}

        return list;
    }

}
