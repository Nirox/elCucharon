/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entitiy.Restaurant;
import Entitiy.beans.Message;
import Entitiy.beans.RestaurantClient;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.Utils;

/**
 *
 * @author inftel02
 */
@Stateless
@Path("entitiy.restaurant")
public class RestaurantFacadeREST extends AbstractFacade<Restaurant> {

    @PersistenceContext(unitName = "RestaurantBookingServerPU")
    private EntityManager em;

    public RestaurantFacadeREST() {
        super(Restaurant.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Restaurant entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") BigDecimal id, Restaurant entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") BigDecimal id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Restaurant find(@PathParam("id") BigDecimal id) {
        return super.find(id);
    }

    @Override
    public List<Restaurant> findAll() {
        List<Restaurant> findAll = super.findAll();
        return findAll;
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<RestaurantClient> findRestaurantClient(){
        List<RestaurantClient> restaurants = new ArrayList<RestaurantClient>();
        for (Restaurant restaurant : this.findAll()){
            restaurants.add(new RestaurantClient(restaurant.getId(), restaurant.getCity(), restaurant.getAddress(), restaurant.getDescription(), restaurant.getImage(), restaurant.getName(), restaurant.getRestaurantSessionList(),restaurant.getPublicKey()));
        }
        return restaurants;
    }
    
    public List<Restaurant> findAllWithRestaurantSession() {
        return getEntityManager().createQuery("Select r FROM Restaurant r LEFT JOIN FETCH r.restaurantSessionList rS", Restaurant.class)
                .getResultList();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Restaurant> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("auth/{id}/{key}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Message getAuth(@PathParam("id") BigDecimal id, @PathParam("key") Boolean key) throws NoSuchAlgorithmException{
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[20];
        Message message = new Message();
        sr.nextBytes(salt);
        message.setMessage(Utils.toHex(salt));
        Restaurant restaurant = this.find(id);
        restaurant.setSecretNumber(message.getMessage());
        if(!key){
            message.setKey(restaurant.getPrivateKey());
        }
        
        return message;
    }
}
