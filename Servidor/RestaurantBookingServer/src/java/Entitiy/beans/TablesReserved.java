/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitiy.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class TablesReserved implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private BigDecimal idSession;
    private BigDecimal tables;
    private String dateReserve;
    
    public TablesReserved(){
        
    }
    
    public TablesReserved(BigDecimal idSession, BigDecimal tables, String dateReserve) {
        this.idSession = idSession;
        this.tables = tables;
        this.dateReserve = dateReserve;
    }
            
    public BigDecimal getIdSession() {
        return idSession;
    }

    public void setIdSession(BigDecimal idSession) {
        this.idSession = idSession;
    }

    public BigDecimal getTables() {
        return tables;
    }

    public void setTables(BigDecimal tables) {
        this.tables = tables;
    }

    public String getDateReserve() {
        return dateReserve;
    }

    public void setDateReserve(String dateReserve) {
        this.dateReserve = dateReserve;
    }
    
    
}
