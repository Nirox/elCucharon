/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitiy.beans;

import Entitiy.RestaurantSession;
import java.math.BigDecimal;
import java.util.List;

public class RestaurantClient {
    private BigDecimal id;
    private String city;
    private String address;
    private String description;
    private String image;
    private String name;
    private List<RestaurantSession> restaurantSessionList;
    private String publicKey;
    
    public RestaurantClient() {
    }

    public RestaurantClient(BigDecimal id, String city, String address, String description, String image, String name, List<RestaurantSession> restaurantSessionList, String publicKey) {
        this.id = id;
        this.city = city;
        this.address = address;
        this.description = description;
        this.image = image;
        this.name = name;
        this.restaurantSessionList = restaurantSessionList;
        this.publicKey=publicKey;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RestaurantSession> getRestaurantSessionList() {
        return restaurantSessionList;
    }

    public void setRestaurantSessionList(List<RestaurantSession> restaurantSessionList) {
        this.restaurantSessionList = restaurantSessionList;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
    
    
    
}
