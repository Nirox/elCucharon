/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitiy.beans;

import java.math.BigDecimal;
import java.math.BigDecimal;
import java.util.Date;

public class ReserveClientRestaurant {
    private String telephone;
    private String dateReserve;
    private BigDecimal tables;
    private String email;
    private String open;

    public ReserveClientRestaurant() {
    }

    public ReserveClientRestaurant(String telephone, String dateReserve, BigDecimal tables, String email, String open) {
        this.telephone = telephone;
        this.dateReserve = dateReserve;
        this.tables = tables;
        this.email = email;
        this.open = open;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDateReserve() {
        return dateReserve;
    }

    public void setDateReserve(String dateReserve) {
        this.dateReserve = dateReserve;
    }

    public BigDecimal getTables() {
        return tables;
    }

    public void setTables(BigDecimal tables) {
        this.tables = tables;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }
    
    
    
}
