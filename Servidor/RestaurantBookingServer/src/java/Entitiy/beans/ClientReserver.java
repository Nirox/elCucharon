/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitiy.beans;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author inftel02
 */
public class ClientReserver {
    private String telephone;
    private Date dateReserve;
    private BigInteger tables;
    private String email;
    private BigDecimal idSession;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getDateReserve() {
        return dateReserve;
    }

    public void setDateReserve(Date dateReserve) {
        this.dateReserve = dateReserve;
    }

    public BigInteger getTables() {
        return tables;
    }

    public void setTables(BigInteger tables) {
        this.tables = tables;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getIdSession() {
        return idSession;
    }

    public void setIdSession(BigDecimal idSession) {
        this.idSession = idSession;
    }
    
    
    
}
