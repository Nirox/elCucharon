/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitiy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author inftel02
 */
@Entity
@Table(name = "RESERVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserve.findAll", query = "SELECT r FROM Reserve r")
    , @NamedQuery(name = "Reserve.findById", query = "SELECT r FROM Reserve r WHERE r.id = :id")
    , @NamedQuery(name = "Reserve.findByTelephone", query = "SELECT r FROM Reserve r WHERE r.telephone = :telephone")
    , @NamedQuery(name = "Reserve.findByDateReserve", query = "SELECT r FROM Reserve r WHERE r.dateReserve = :dateReserve")
    , @NamedQuery(name = "Reserve.findByTables", query = "SELECT r FROM Reserve r WHERE r.tables = :tables")
    , @NamedQuery(name = "Reserve.findByEmail", query = "SELECT r FROM Reserve r WHERE r.email = :email")})
@SequenceGenerator(name="reservesequence", initialValue=1, allocationSize=10)
public class Reserve implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="reservesequence")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(max = 1500)
    @Column(name = "TELEPHONE")
    private String telephone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_RESERVE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReserve;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TABLES")
    private BigInteger tables;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "EMAIL")
    private String email;
    @JoinColumn(name = "ID_SESSION", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private RestaurantSession idSession;

    public Reserve() {
    }

    public Reserve(BigDecimal id) {
        this.id = id;
    }

    public Reserve(BigDecimal id, String telephone, Date dateReserve, BigInteger tables, String email) {
        this.id = id;
        this.telephone = telephone;
        this.dateReserve = dateReserve;
        this.tables = tables;
        this.email = email;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getDateReserve() {
        return dateReserve;
    }

    public void setDateReserve(Date dateReserve) {
        this.dateReserve = dateReserve;
    }

    public BigInteger getTables() {
        return tables;
    }

    public void setTables(BigInteger tables) {
        this.tables = tables;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @XmlTransient
    public RestaurantSession getIdSession() {
        return idSession;
    }

    public void setIdSession(RestaurantSession idSession) {
        this.idSession = idSession;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserve)) {
            return false;
        }
        Reserve other = (Reserve) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entitiy.Reserve[ id=" + id + " ]";
    }
    
}
