/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitiy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author inftel02
 */
@Entity
@Table(name = "RESTAURANT_SESSION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RestaurantSession.findAll", query = "SELECT r FROM RestaurantSession r")
    , @NamedQuery(name = "RestaurantSession.findByOpen", query = "SELECT r FROM RestaurantSession r WHERE r.open = :open")
    , @NamedQuery(name = "RestaurantSession.findById", query = "SELECT r FROM RestaurantSession r WHERE r.id = :id")
    , @NamedQuery(name = "RestaurantSession.findByClose", query = "SELECT r FROM RestaurantSession r WHERE r.close = :close")
    , @NamedQuery(name = "RestaurantSession.findByTables", query = "SELECT r FROM RestaurantSession r WHERE r.tables = :tables")
    , @NamedQuery(name = "RestaurantSession.findByPeopleTable", query = "SELECT r FROM RestaurantSession r WHERE r.peopleTable = :peopleTable")})
public class RestaurantSession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPEN")
    private String open;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLOSE")
    private String close;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TABLES")
    private BigInteger tables;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PEOPLE_TABLE")
    private BigInteger peopleTable;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSession")
    private List<Reserve> reserveList;
    @JoinColumn(name = "ID_RESTAURANT", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Restaurant idRestaurant;
    

    public RestaurantSession() {
    }

    public RestaurantSession(BigDecimal id) {
        this.id = id;
    }

    public RestaurantSession(BigDecimal id, String open, String close, BigInteger tables, BigInteger peopleTable) {
        this.id = id;
        this.open = open;
        this.close = close;
        this.tables = tables;
        this.peopleTable = peopleTable;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public BigInteger getTables() {
        return tables;
    }

    public void setTables(BigInteger tables) {
        this.tables = tables;
    }

    public BigInteger getPeopleTable() {
        return peopleTable;
    }

    public void setPeopleTable(BigInteger peopleTable) {
        this.peopleTable = peopleTable;
    }

    @XmlTransient
    public List<Reserve> getReserveList() {
        return reserveList;
    }

    public void setReserveList(List<Reserve> reserveList) {
        this.reserveList = reserveList;
    }

    @XmlTransient
    public Restaurant getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(Restaurant idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RestaurantSession)) {
            return false;
        }
        RestaurantSession other = (RestaurantSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entitiy.RestaurantSession[ id=" + id + " ]";
    }
    
}
