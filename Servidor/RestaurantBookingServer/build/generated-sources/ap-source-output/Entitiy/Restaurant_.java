package Entitiy;

import Entitiy.RestaurantSession;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-02-16T21:56:43")
@StaticMetamodel(Restaurant.class)
public class Restaurant_ { 

    public static volatile SingularAttribute<Restaurant, String> image;
    public static volatile SingularAttribute<Restaurant, String> privateKey;
    public static volatile SingularAttribute<Restaurant, String> address;
    public static volatile SingularAttribute<Restaurant, String> city;
    public static volatile SingularAttribute<Restaurant, String> secretNumber;
    public static volatile SingularAttribute<Restaurant, String> name;
    public static volatile SingularAttribute<Restaurant, String> description;
    public static volatile SingularAttribute<Restaurant, BigDecimal> id;
    public static volatile ListAttribute<Restaurant, RestaurantSession> restaurantSessionList;
    public static volatile SingularAttribute<Restaurant, String> publicKey;

}