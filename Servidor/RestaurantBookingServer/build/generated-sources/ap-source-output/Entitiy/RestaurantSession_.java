package Entitiy;

import Entitiy.Reserve;
import Entitiy.Restaurant;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-02-16T21:56:43")
@StaticMetamodel(RestaurantSession.class)
public class RestaurantSession_ { 

    public static volatile SingularAttribute<RestaurantSession, BigInteger> peopleTable;
    public static volatile ListAttribute<RestaurantSession, Reserve> reserveList;
    public static volatile SingularAttribute<RestaurantSession, BigInteger> tables;
    public static volatile SingularAttribute<RestaurantSession, Restaurant> idRestaurant;
    public static volatile SingularAttribute<RestaurantSession, BigDecimal> id;
    public static volatile SingularAttribute<RestaurantSession, String> close;
    public static volatile SingularAttribute<RestaurantSession, String> open;

}