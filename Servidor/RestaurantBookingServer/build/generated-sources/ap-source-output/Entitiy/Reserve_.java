package Entitiy;

import Entitiy.RestaurantSession;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-02-16T21:56:43")
@StaticMetamodel(Reserve.class)
public class Reserve_ { 

    public static volatile SingularAttribute<Reserve, Date> dateReserve;
    public static volatile SingularAttribute<Reserve, BigInteger> tables;
    public static volatile SingularAttribute<Reserve, String> telephone;
    public static volatile SingularAttribute<Reserve, BigDecimal> id;
    public static volatile SingularAttribute<Reserve, RestaurantSession> idSession;
    public static volatile SingularAttribute<Reserve, String> email;

}