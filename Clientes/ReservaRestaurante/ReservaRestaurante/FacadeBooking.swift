//
//  DAOBooking.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 31/1/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

class FacadeBooking{
    var urlBase : String = "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.reserve/"
    
    func create (booking : Booking, idSession: CUnsignedLong, publicKey : String, handler: @escaping ( _ : String) -> Void){
        //request to create object in ddbb
        let calendar = Calendar(identifier: .gregorian)
        var componentsDate = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: (booking.date)!)
        
        if let day = componentsDate.day, let month = componentsDate.month, let year = componentsDate.year {

            let date = "\(year)-\(String(format: "%02d", month))-\(String(format: "%02d", day))"
            
            var cipheredTelephone = ""
            
            var security = Security()
            
            
            cipheredTelephone = security.cipher( publicKeyString: publicKey, telephone: booking.telephone!)

            
            
            
            let jsonObject: [String: Any] = [
                "telephone": cipheredTelephone,
                "email": booking.email!,
                "tables":booking.tables!,
                "dateReserve":date+"T19:25:03+01:00",
                "idSession": idSession
            ]
            self.request(request: "", booking: jsonObject, handler: handler)
        }
 
    }
    
    private func request (request: String,booking: [String: Any] ,handler: @escaping ( _ : String) -> Void){
        let session = URLSession(configuration: .default)
        let url = URL(string: urlBase + request)
        let jsonData = try? JSONSerialization.data(withJSONObject: booking, options: .prettyPrinted)
        
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.httpBody = jsonData
        //creating a dataTask
        let task = session.dataTask(with: request) { (data, response, error) in
            //if there is any error
            if let e = error {
                //displaying the message
                fatalError("Error Occurred: \(e)")
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    //checking if the response contains an image
                    if (data != nil) {
                        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                        print(json)
                        if let elements = json as? [String: String] {
                            handler(elements["message"]!)
                        }else{
                            fatalError("not valid json")
                        }
                    } else {
                        fatalError("not valid request")
                    }
                } else {
                    fatalError("No response from server")
                }
            }
            
        }
        task.resume()
    }
    
}

