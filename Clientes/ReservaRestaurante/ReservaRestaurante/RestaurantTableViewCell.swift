//
//  BarTableViewCell.swift
//  listaBares
//
//  Created by INFTEL 10 on 29/1/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var restaurantImage: UIImageView?
    @IBOutlet weak var nameRestaurantLabel: UILabel!
    
    @IBOutlet weak var direccionRestaurantLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
