
import Foundation
import Security

class Security{
    
    public func cipher(publicKeyString: String,  telephone:String) -> String {
        
        let ccAvailable : Bool = CC.available()
       
        let publicKeyDER = try? SwKeyConvert.PublicKey.pemToPKCS1DER(publicKeyString)
        
        let cipheredData = try? CC.RSA.encrypt(telephone.data(using: .utf8)!, derKey: publicKeyDER!, tag: Data(), padding: .oaep, digest: .sha256)
        return cipheredData!.hexadecimalString()
        
    }
}

