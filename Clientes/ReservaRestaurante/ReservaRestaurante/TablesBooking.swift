//
//  TablesBooking.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 2/2/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

class TablesBooking {
    var idSession : CUnsignedLong
    var tables: CUnsignedInt
    var date: String
    
    init?(idSession: CUnsignedLong, tables: CUnsignedInt, date: String){
        
        self.idSession = idSession
        self.tables = tables
        self.date = date
    }
    
}
