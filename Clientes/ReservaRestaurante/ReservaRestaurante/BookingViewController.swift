//
//  ViewController.swift
//  proyectoEntrega
//
//  Created by INFTEL 02 on 28/1/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class BookingViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var restaurant  : Restaurant?
    var sessions = [Session]()
    private var hours = [String]()
    private var booking : Booking?
    private var selectedSession = 0
    var tablesBooking : [TablesBooking]?
    private var selectedTableBooking : TablesBooking?;
    
    // CoreData variables
    var user: User?
    var users:[User]? = nil
    
    @IBOutlet weak var availableTablesText: UITextField!
    @IBOutlet weak var peopleText: UITextField!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerDateView: UIDatePicker!
    @IBOutlet weak var bookingButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg2")!)
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg2")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        booking = Booking()
        
        availableTablesText.text = "\(sessions[0].tables)"
        
        for session in sessions {
            hours.append(session.open)
        }
        
        emailText.delegate = self
        contactNumber.delegate = self
        addDoneButtonOnKeyboard()
        
        pickerDateView.minimumDate = Date()
        pickerDateView.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        pickerView.delegate = self
        pickerView.dataSource = self
        
        self.formatDate(hourPicker: hours[selectedSession])
        self.formatDate(sender: pickerDateView)
        self.calculateTablesReserved()
        
        user = CoreDataHandler.fetchLastObject()
        
        if (user != nil){
            contactNumber.text = user?.contact
            emailText.text = user?.email
        }
        
        if contactNumber.text == "" || emailText.text == ""{
            self.bookingButton.isEnabled = false
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return hours[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return hours.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedSession = row
        formatDate(hourPicker: hours[row])
        self.calculateTablesReserved()
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        formatDate(sender: sender)
        self.calculateTablesReserved()
    }
    
    @objc func doneButtonAction(_ sender: Any){
        if contactNumber.text != "" && emailText.text != "" {
            self.bookingButton.isEnabled = true
        }
        contactNumber.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func bookingButton(_ sender: UIButton) {
        booking?.telephone = contactNumber.text!
        booking?.email = emailText.text
        
        let found = CoreDataHandler.findUser(number: contactNumber.text!)
        
        if !found {
            //User with telephone not registered
            
            let alertController = UIAlertController(title: "Verificación de la reserva", message: "Introduce el código recibido en el SMS", preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "Aceptar", style: .default) { (_) in
                
                let value = alertController.textFields?[0].text
                if value == "1111"{
                    //correct value
                    CoreDataHandler.saveObject(contact: self.contactNumber.text!, email: self.emailText.text!)
                    self.createReserve()
                }else{
                    self.createSimpleDialog(title: "Reserva no realizada", message: "El número de verificación no es correcto")
                }
                
            }
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (_) in }
            
            alertController.addTextField { (textField) in
                textField.placeholder = "Código"
                textField.keyboardType = UIKeyboardType.numberPad
            }
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            self.createReserve()
        }
    }
    
    @IBAction func moreLessPeople(_ sender: UIStepper) {
        peopleText.text = "\(Int(sender.value))"
        booking?.tables = CUnsignedInt(ceil(Double(sender.value) / Double((sessions[selectedSession].peoplePerTable))))
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailText.resignFirstResponder()
        contactNumber.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.bookingButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if contactNumber.text != "" && emailText.text != ""{
            self.bookingButton.isEnabled = true
        }
    }
    
    //MARK: privates function
    private func createReserve(){
        let daoBooking = FacadeBooking()
        daoBooking.create(booking: self.booking!, idSession: self.sessions[self.selectedSession].id, publicKey: (self.restaurant?.publicKey)!){ (message) in
            DispatchQueue.main.async {
                if message == "OK"{
                    
                    //A new alert showing that booking was successfully done
                    self.createSimpleDialog(title: "Reserva realizada", message: "Se ha enviado un mensaje con la información de la reserva al correo indicado")
                    
                    if(self.selectedTableBooking != nil){
                        self.selectedTableBooking?.tables += (self.booking?.tables)!
                        self.availableTablesText.text = "\(self.sessions[self.selectedSession].tables - (self.selectedTableBooking?.tables)!)"
                    }else{
                        self.availableTablesText.text = "\(Int(self.availableTablesText.text!)! - Int((self.booking?.tables)!))"
                    }
                }else{
                    self.createSimpleDialog(title: "No se ha realizado la reserva", message: "No hay mesas disponibles para la fecha y hora seleccionada")
                }
            }
        }
    }
    
    private func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction(_:)))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.contactNumber.inputAccessoryView = doneToolbar
        
    }
    
    private func formatDate(sender: UIDatePicker){
        let calendar = Calendar(identifier: .gregorian)
        
        var componentsDate = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: (booking?.date)!)
        var componentsSender = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: sender.date)
        
        componentsDate.year = componentsSender.year
        componentsDate.month = componentsSender.month
        componentsDate.day = componentsSender.day
        
        booking?.date = calendar.date(from: componentsDate)!
    }
    
    private func formatDate(hourPicker : String){
        let calendar = Calendar(identifier: .gregorian)
        
        var componentsDate = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: (booking?.date)!)
        var date = hourPicker.components(separatedBy: ":")
        
        if date.count == 2 {
            componentsDate.hour = Int(date[0])
            componentsDate.minute = Int(date[1])
        } else {
            fatalError("date in spicker not valid")
        }
        
        booking?.date = calendar.date(from: componentsDate)!
    }
    
    private func calculateTablesReserved(){
        let calendar = Calendar(identifier: .gregorian)
        
        availableTablesText.text = "\(sessions[selectedSession].tables)"
        selectedTableBooking = nil
        var componentsDate = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: (booking?.date)!)
        if (self.tablesBooking != nil){
            for reserved in self.tablesBooking! {
                if let day = componentsDate.day, let month = componentsDate.month, let year = componentsDate.year {
                    let date = "\(String(format: "%02d", day))-\(String(format: "%02d", month))-\(year)"
                    if sessions[selectedSession].id == reserved.idSession && reserved.date == date{
                        availableTablesText.text = "\(sessions[selectedSession].tables - reserved.tables)"
                        selectedTableBooking = reserved;
                    }
                }
            }
        }
    }
    
    private func createSimpleDialog(title: String, message : String){
        let alertConfirmationController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let confirmBookingAction = UIAlertAction(title: "Aceptar", style: .default) { (_) in}
        
        alertConfirmationController.addAction(confirmBookingAction)
        self.present(alertConfirmationController, animated: true, completion: nil)
    }
}

