//
//  DAOTablesBooking.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 2/2/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//
import UIKit

class FacadeTablesBooking{
    var urlBase : String = "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.reserve/"
    
    func getList(restaurant: Restaurant, handler: @escaping (_ : [TablesBooking]) -> Void){
        request(request: "tables/\(restaurant.id)", handler: handler)
    }
    
    private func request (request: String,handler: @escaping ( _ : [TablesBooking]) -> Void){
        let session = URLSession(configuration: .default)
        let url = URL(string: urlBase + request)
        
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        //creating a dataTask
        let task = session.dataTask(with: request) { (data, response, error) in
            
            //if there is any error
            if let e = error {
                //displaying the message
                print("Error Occurred: \(e)")
                
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    //checking if the response contains an image
                    if (data != nil) {
                        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                        print(json)
                        if let elements = json as? [[String: Any]] {
                            handler(self.jsonToTablesBooking(data: elements))
                        }else{
                            fatalError("not valid json")
                        }
                        
                    } else {
                        print("not valid request")
                    }
                } else {
                    print("No response from server")
                }
            }
            
        }
        task.resume()
    }
    
    func jsonToTablesBooking(data: [[String : Any]]) -> [TablesBooking] {
        var tables = [TablesBooking]()
        for object in data {
            guard let id = object["idSession"] as? CUnsignedLong else{
                fatalError("json not valid")
            }
            
            guard let table = object["tables"] as? CUnsignedInt else{
                fatalError("json not valid")
            }
            
            guard let date = object["dateReserve"] as? String else{
                fatalError("json not valid")
            }
            tables.append(TablesBooking(idSession: id, tables: table, date: date)!)
        }
        return tables
    }
}
