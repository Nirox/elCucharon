//
//  BarTableViewController.swift
//  listaBares
//
//  Created by INFTEL 10 on 29/1/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController {
    
    
    var restaurants = [Restaurant]()
    var tablesBooking = [TablesBooking]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dao = FacadeRestaurant()
        dao.getList(handler: updateRestaurantList, dataCharge: updateView)
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg2")!)
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg2")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
    private func updateRestaurantList (restaurants : [Restaurant]){
        self.restaurants = restaurants
        self.tableView.reloadData()
    }
    
    private func updateView(resturant :Restaurant){
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return restaurants.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "RestaurantTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? RestaurantTableViewCell  else {
            fatalError("The dequeued cell is not an instance of RestaurantTableViewCell.")
        }
    
        let restaurant = restaurants[indexPath.row]
        
        cell.nameRestaurantLabel.text = restaurant.name
        cell.direccionRestaurantLabel.text = restaurant.address + ", " + restaurant.city
        cell.restaurantImage?.image = restaurant.image
        
        cell.restaurantImage?.backgroundColor = UIColor.black
        cell.restaurantImage?.layer.cornerRadius = (cell.restaurantImage?.frame.size.width)! / 2
        cell.restaurantImage?.clipsToBounds = true
       
        return cell
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        guard let viewController = segue.destination as? ViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        
        guard let selectedRestaurantCell = sender as? RestaurantTableViewCell else {
            fatalError("Unexpected sender: \(String(describing: sender))")
        }
        
        guard let indexPath = tableView.indexPath(for: selectedRestaurantCell) else {
            fatalError("The selected cell is not being displayed by the table")
        }
        
        viewController.restaurant  = restaurants[indexPath.row]
        viewController.tablesBooking  = self.tablesBooking
        let daoTablesBooking = FacadeTablesBooking()
        
        daoTablesBooking.getList(restaurant: restaurants[indexPath.row]){ (tablesBooking) in
                DispatchQueue.main.async {
                    viewController.tablesBooking  = tablesBooking
                }
        }
    }
    
}
