//
//  Session.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 30/1/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

class Session {
    var id : CUnsignedLong
    var open: String
    var close: String
    var tables: CUnsignedInt
    var peoplePerTable: CUnsignedInt
    var books: [Booking]
    
    init?(id: CUnsignedLong, open: String, close: String, tables: CUnsignedInt, peoplePerTable: CUnsignedInt, books: [Booking] ){
        
        self.id = id
        self.open = open
        self.close = close
        self.tables = tables
        self.peoplePerTable = peoplePerTable
        self.books = books
        
    }
}
