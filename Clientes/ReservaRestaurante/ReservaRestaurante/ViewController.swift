//
//  ViewController.swift
//  RestaurantApp
//
//  Created by INFTEL 05 on 28/1/18.
//  Copyright © 2018 INFTEL 05. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var restaurant  : Restaurant?
    var tablesBooking : [TablesBooking]?
    
  //Mark propreties
    @IBOutlet weak var etiquetaNombreRestaurante: UILabel!
    @IBOutlet weak var etiquetaDescripcionRestaurante: UILabel!
    @IBOutlet weak var etiquetaLocalizacion: UILabel!
    @IBOutlet weak var NombreDescripcion: UITextView!
    @IBOutlet weak var NombreCampoLocalizacion: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiquetaNombreRestaurante.text = restaurant?.name
        NombreCampoLocalizacion.text = (restaurant?.address)! + ", " + (restaurant?.city)!
        NombreDescripcion.text = restaurant?.description
        photoImageView.image = restaurant?.image
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg1")!)
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg1")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        self.photoImageView.backgroundColor = UIColor.black
        self.photoImageView.layer.cornerRadius = 13.0
        self.photoImageView.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        guard let bookingViewController = segue.destination as? BookingViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        bookingViewController.restaurant = restaurant
        bookingViewController.sessions  = (restaurant?.sessions)!
        bookingViewController.tablesBooking = self.tablesBooking
    }
}

