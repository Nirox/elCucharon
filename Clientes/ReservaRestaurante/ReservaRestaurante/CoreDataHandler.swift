//
//  CoreDataHandler.swift
//  ReservaRestaurante
//
//  Created by INFTEL 10 on 1/2/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHandler: NSObject {
    
    private class func getContext() -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func saveObject(contact: String, email: String ) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(email, forKey: "email")
        manageObject.setValue(contact, forKey: "contact")
        
        do {
            try context.save()
            return true
        }catch {
            return false
        }
        
    }
    class func fetchObject() -> [User]? {
        let context = getContext()
        var users:[User]? = nil
        
        
        do{
            users = try context.fetch(User.fetchRequest())
            return users
            
        } catch {
            return users
        }
    }
    
    class func fetchLastObject() -> User? {
        
        let context = getContext()
        var users:[User]? = nil
        var user:User? = nil
        
        do{
            users = try context.fetch(User.fetchRequest())
            for i in users! {
                print(i.email! + i.contact!)
            }
            
            if users?.count == 0 {
                return user
            } else{
                user = users![((users?.count)!-1)]
                print((user?.email)! + "  " + (user?.contact)!)
                return user
                
            }
        } catch {
            return users![(users?.count)!]
        }
    }
    
    class func findUser(number : String) -> Bool{
        var users = CoreDataHandler.fetchObject()
        
        var found:Bool = false
        var i = 0
        while (!found && i < (users?.count)!){
            if(number == users![i].contact){
                found = true
            }
            i = i + 1
        }
        return found
    }
    
}
