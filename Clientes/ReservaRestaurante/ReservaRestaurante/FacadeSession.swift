//
//  DAOSession.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 31/1/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

class FacadeSession{
    var urlBase : String = "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.restaurant/"
    
    func getList(handler: @escaping (_ : [Session]) -> Void){
        request(request: "", handler: handler)
    }
    
    private func request (request: String ,handler: @escaping ( _ : [Session]) -> Void){
        let session = URLSession(configuration: .default)
        let url = URL(string: urlBase + request)
        
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        //creating a dataTask
        let task = session.dataTask(with: request) { (data, response, error) in
            
            //if there is any error
            if let e = error {
                //displaying the message
                print("Error Occurred: \(e)")
                
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    //checking if the response contains an image
                    if (data != nil) {
                        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                        print(json)
                        if let elements = json as? [[String: Any]] {
                            handler(self.jsonToSession(data: elements))
                        }else{
                            fatalError("not valid json")
                        }
                        
                    } else {
                        print("not valid request")
                    }
                } else {
                    print("No response from server")
                }
            }
            
        }
        task.resume()
    }
    
    func jsonToSession(data: [[String : Any]]) -> [Session] {
        var daoBooking = FacadeBooking()
        var sessions = [Session]()
        for object in data {
            guard let open = object["open"] as? String else{
                fatalError("json not valid")
            }
            
            guard let close = object["close"] as? String else{
                fatalError("json not valid")
            }
            
            guard let tables = object["tables"] as? CUnsignedInt else{
                fatalError("json not valid")
            }
            
            guard let peoplePerTable = object["peopleTable"] as? CUnsignedInt else{
                fatalError("json not valid")
            }
            
            guard let id = object["id"] as? CUnsignedLong else{
                fatalError("json not valid")
            }
            
            let books = [Booking]()
            sessions.append(Session(id: id, open: open, close: close, tables: tables, peoplePerTable: peoplePerTable, books: books)!)
        }
        return sessions
    }
}

