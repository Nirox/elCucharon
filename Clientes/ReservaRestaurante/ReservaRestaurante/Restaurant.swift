//
//  Bar.swift
//  listaBares
//
//  Created by INFTEL 10 on 29/1/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//

import UIKit

class Restaurant {
    var id: CUnsignedLong
    var name: String
    var address: String
    var image: UIImage?
    var imageURL: String?
    var description: String
    var city: String
    var sessions : [Session]
    var publicKey : String;
    
    // MARK: Initialization
    init?(id: CUnsignedLong, name: String, address:String, imageURL: String?, description: String, city: String, sessions : [Session], publicKey: String){
        if name.isEmpty || address.isEmpty || description.isEmpty || city.isEmpty {
            return nil
        }
        
        self.id = id
        self.name = name
        self.address = address
        self.imageURL = imageURL
        self.description = description
        self.city = city
        self.sessions = sessions
        self.publicKey = publicKey
    }
    
}
