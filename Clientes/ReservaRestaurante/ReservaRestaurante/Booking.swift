//
//  Booking.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 30/1/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

class Booking {
    var id : CUnsignedLong?
    var telephone: String?
    var date: Date?
    var tables: CUnsignedInt?
    var email: String?
    
    init?(id: CUnsignedLong, telephone: String, date: Date, tables: CUnsignedInt, email: String ){
        
        self.id = id
        self.telephone = telephone
        self.date = date
        self.tables = tables
        self.email = email
        
    }
    
    init?(){
        self.id = nil
        self.date = Date()
        self.tables = 1
    }
}
