//
//  DAORestaurant.swift
//  ReservaRestaurante
//
//  Created by INFTEL 02 on 29/1/18.
//  Copyright © 2018 Apple Inc. All rights reserved.
//

import UIKit

class FacadeRestaurant{
    var urlBase : String = "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.restaurant/"
    
    func getList(handler: @escaping (_ : [Restaurant]) -> Void, dataCharge: @escaping ( _ : Restaurant) -> Void){
        request(request: "", handler: handler, dataCharge: dataCharge)
    }
    
    private func request (request: String ,handler: @escaping ( _ : [Restaurant]) -> Void, dataCharge: @escaping ( _ : Restaurant) -> Void){
        let session = URLSession(configuration: .default)
        let url = URL(string: urlBase + request)
        
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        //creating a dataTask
        let task = session.dataTask(with: request) { (data, response, error) in
            
            //if there is any error
            if let e = error {
                //displaying the message
                print("Error Occurred: \(e)")
                
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    //checking if the response contains an image
                    if (data != nil) {
                        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                        print(json)
                        if let elements = json as? [[String: Any]] {
                            let restaurants = self.jsonToRestaurant(data: elements)
                            DispatchQueue.main.async {
                                for restaurant in restaurants {
                                    self.chargeImage(restaurant: restaurant, completionHandler: dataCharge)
                                }
                                handler(restaurants)
                            }
                        }else{
                            fatalError("not valid json")
                        }
                        
                    } else {
                        print("not valid request")
                    }
                } else {
                    print("No response from server")
                }
            }
            
        }
        task.resume()
    }
    
    func chargeImage(restaurant: Restaurant, completionHandler: @escaping ( _ : Restaurant) -> Void){
        let session = URLSession(configuration: .default)
        let url = URL(string: restaurant.imageURL!)
        
        //creating a dataTask
        let getImageFromUrl = session.dataTask(with: url!) { (data, response, error) in
            
            //if there is any error
            if let e = error {
                //displaying the message
                print("Error Occurred: \(e)")
                
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    
                    //checking if the response contains an image
                    if let imageData = data {
                        restaurant.image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            completionHandler(restaurant)
                        }
                        
                    } else {
                        print("Image file is currupted")
                    }
                } else {
                    print("No response from server")
                }
            }
        }
        
        //starting the download task
        getImageFromUrl.resume()
    }
    
    private func jsonToRestaurant(data: [[String : Any]]) -> [Restaurant] {
        var restaurants = [Restaurant]()
        let daoSession = FacadeSession()
        for object in data {
            guard let city = object["city"] as? String else{
                fatalError("json not valid")
            }
            
            guard let name = object["name"] as? String else{
                fatalError("json not valid")
            }
            
            guard let address = object["address"] as? String else{
                fatalError("json not valid")
            }
            
            guard let description = object["description"] as? String else{
                fatalError("json not valid")
            }
            
            guard let image = object["image"] as? String else{
                fatalError("json not valid")
            }
            
            guard let id = object["id"] as? CUnsignedLong else{
                fatalError("json not valid")
            }
            var publicKey = ""
            if ((object["publicKey"] as? String) != nil) {
                publicKey = object["publicKey"] as! String
            }
            let sessions = daoSession.jsonToSession(data: object["restaurantSessionList"] as! [[String : Any]])
            restaurants.append(Restaurant(id: id, name: name, address: address, imageURL: image, description: description, city: city, sessions: sessions, publicKey: publicKey)!)
        }
        return restaurants
    }
}
