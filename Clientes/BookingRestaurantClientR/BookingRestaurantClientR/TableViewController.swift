//
//  ViewController.swift
//  OnlyAViewVersion2
//
//  Created by INFTEL 10 on 14/2/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    @IBOutlet weak var tittleRestaurant: UILabel!
    @IBOutlet weak var tittleDayLabel: UILabel!
    @IBOutlet weak var tittleHourLabel: UILabel!
    @IBOutlet weak var tittleTablesLabel: UILabel!
    @IBOutlet weak var tittleTelephoneLabel: UILabel!
    
    @IBOutlet weak var tittleEmailLabel: UILabel!
    
    
    var bookings = [Booking]()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let security = SecurityFacade()
        security.auth(handler: updateBookingList)
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg2")!)
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg2")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)

    }
    
    
    private func updateBookingList (bookings : [Booking]){
        DispatchQueue.main.async {
            self.bookings = bookings
            self.tableView.reloadData()
        }
    }
    
    func getMasterPassword (security: SecurityFacade) -> Void {
        let alertController = UIAlertController(title: "Introducir Clave Maestra", message: "La Clave Maestra es necesaría para obtener los datos de la reserva", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Aceptar", style: .default) { (_) in
            let value = (alertController.textFields?[0].text)!
        }
        
        alertController.addAction(confirmAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bookings.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "BookingTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? BookingTableViewCell  else {
            fatalError("The dequeued cell is not an instance of BookingTableViewCell.")
        }
        
        let booking = bookings[indexPath.row]
        
        
        cell.dayLabel.text = booking.date
        cell.hourLabel.text = booking.hour
        cell.tablesLabel!.text = String(describing: booking.tables!)
        cell.telephoneLabel!.text = String(describing: booking.telephone!) //as String
        cell.emailLabel.text = booking.email
        
        return cell
    }

}


