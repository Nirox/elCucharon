//
//  SecurityFacade.swift
//  OnlyAViewVersion2
//
//  Created by INFTEL 10 on 15/2/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//


import Foundation
import UIKit



class SecurityFacade {
    var privateKey : Data?
    let tag = "restaurantkey"
    var urlBase = ""
    
    private func doIHavePrivateKey () -> (Data?) {
        let ccAvailable : Bool = CC.available()
        
        let privateKeyPEM = try? SwKeyStore.getKey(self.tag)
        
        if privateKeyPEM != nil {
            self.privateKey = try? SwKeyConvert.PrivateKey.pemToPKCS1DER(privateKeyPEM!)
            return privateKey
        }else{
            return nil
        }

    }
    
    
    private func setUrl (flag: Bool) -> String {
        if flag {
            return "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.restaurant/auth/5/true"
        } else {
            return "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.restaurant/auth/5/false"
        }
    }

    private func request (key : Bool, handler: @escaping ( _ : ServerResponse) -> Void){
        let session = URLSession(configuration: .default)
        let urlKey = self.setUrl(flag: key)
        let url = URL(string: urlKey)
        
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        //creating a dataTask
        let task = session.dataTask(with: request) { (data, response, error) in
            
            //if there is any error
            if let e = error {
                //displaying the message
                print("Error Occurred: \(e)")
                
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    //checking if the response contains an image
                    if (data != nil) {
                        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                        print(json!)
                        if let elements = json as? [String: Any] {
                            let serverResponse = self.jsonToServerResponse(data: elements)

                            handler(serverResponse)
                            
                        }else{
                            print("..../1/2")
                            fatalError("not valid json")
                        }
                        
                    } else {
                        print("not valid request")
                    }
                } else {
                    print("No response from server")
                }
            }
            
        }
        task.resume()
    }
    
    
    
    private func jsonToServerResponse(data: [String : Any]) -> ServerResponse {
        guard let message = data["message"] as? String else{
                fatalError("json not valid")
            }
        guard let cPK = data["key"] as? String else{
                fatalError("json not valid")
            }
        
        return ServerResponse(message: message, cPK: cPK)!
    }
    
    func decipherTelephone(telephoneCipher: String, privateKey: Data) -> String {
        let ccAvailable : Bool = CC.available()
        let decipher = try? CC.RSA.decrypt(telephoneCipher.dataFromHexadecimalString()!, derKey: privateKey, tag: Data(), padding: .oaep, digest: .sha256)
        
        return (String(data: (decipher?.0)!, encoding: .utf8))!
    }
    
    func auth(handler: @escaping (_ : [Booking]) -> Void) {
        let _ : Bool = CC.available()
        self.privateKey = self.doIHavePrivateKey()
        
        self.request( key: privateKey != nil) {(serverResponse) in
            
            if(self.privateKey == nil){
            
                let privateKeyDecipher = try? SwKeyConvert.PrivateKey.decryptPEM(serverResponse.cipheredPrivateKey, passphrase: "longpassword")
            
                try? SwKeyStore.upsertKey(privateKeyDecipher!, keyTag: self.tag, options: [kSecAttrAccessible:kSecAttrAccessibleWhenUnlockedThisDeviceOnly])

                self.privateKey = try? SwKeyConvert.PrivateKey.pemToPKCS1DER(privateKeyDecipher!)
            }
            
            //firmar con la privada
            let signature = try? CC.RSA.sign(serverResponse.message.dataFromHexadecimalString()!, derKey: self.privateKey!, padding: .pss,digest: .sha256, saltLen: 16)
            
            //enviar firma
            let dao = BookingFacade()
            dao.getList(signature: (signature?.hexadecimalString())!, privateKey: self.privateKey!, handler: handler)
            
            
        }

    }
    
}




