//
//  BookingFacade.swift
//  OnlyAViewVersion2
//
//  Created by INFTEL 10 on 14/2/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//

import Foundation
import UIKit



class BookingFacade{
    
    var urlBase : String = "https://niroxinftel.sytes.net/RestaurantBookingServer/webresources/entitiy.reserve/restaurant/5/"
    
    func getList(signature: String, privateKey: Data, handler: @escaping (_ : [Booking]) -> Void){
        request(signature: signature, privateKey: privateKey, handler: handler)
    }
    
    private func request (signature: String, privateKey: Data ,handler: @escaping ( _ : [Booking])-> Void ){
        let session = URLSession(configuration: .default)
        let url = URL(string: urlBase + signature)
        
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        //creating a dataTask
        let task = session.dataTask(with: request) { (data, response, error) in
            
            //if there is any error
            if let e = error {
                //displaying the message
                print("Error Occurred: \(e)")
                
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    //checking if the response contains an image
                    if (data != nil) {
                        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                        print(json!)
                        if let elements = json as? [[String: Any]] {
                            let bookings = self.jsonToRestaurant(data: elements, privateKey: privateKey)
                            handler(bookings)
                            
                        }else{
                            print("..../1/2")
                            fatalError("not valid json")
                        }
                        
                    } else {
                        print("not valid request")
                    }
                } else {
                    print("No response from server")
                }
            }
            
        }
        task.resume()
    }
    
    
    
    private func jsonToRestaurant(data: [[String : Any]], privateKey: Data) -> [Booking] {
        var bookings = [Booking]()
        let facade = SecurityFacade()
        
        for object in data {
            guard let dateBooking = object["dateReserve"] as? String else{
                fatalError("json not valid")
            }
            guard let email = object["email"] as? String else{
                fatalError("json not valid")
            }
            
            guard let hour = object["open"] as? String else{
                fatalError("json not valid")
            }
            guard let tables = object["tables"] as? CUnsignedInt else{
                fatalError("json not valid")
            }
            guard let cipheredTelephone = object["telephone"] as? String else{
                fatalError("json not valid")
            }

            
            bookings.append(Booking(date: dateBooking, email: email, hour: hour, tables: tables, telephone: facade.decipherTelephone(telephoneCipher: cipheredTelephone, privateKey: privateKey))!)
        }
        return bookings
    }
}

