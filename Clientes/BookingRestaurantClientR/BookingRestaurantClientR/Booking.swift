//
//  Booking.swift
//  OnlyAView
//
//  Created by INFTEL 10 on 14/2/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//

import Foundation
import UIKit

class Booking {
    /*
     dateReserve: "2018-02-02T19:25:03+01:00",
     email: "antonio_23@gmail.com",
     open: "13:00",
     tables: 1,
     telephone: 63
     
     */
    var date: String?
    var email: String?
    var hour: String?
    var tables: CUnsignedInt?
    var telephone: String?
    
    
    
    init?(date: String, email: String, hour: String ,  tables: CUnsignedInt , telephone: String ){
        
        self.date = date
        self.email = email
        self.hour = hour
        self.tables = tables
        self.telephone = telephone
        
    }
    
    init?(){
        
        self.date = ""
        self.email = ""
        self.email = ""
        self.tables = 0
        self.telephone = ""
        
    }
}



