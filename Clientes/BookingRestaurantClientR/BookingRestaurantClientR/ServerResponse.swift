//
//  ServerResponse.swift
//  OnlyAViewVersion2
//
//  Created by INFTEL 10 on 15/2/18.
//  Copyright © 2018 INFTEL 10. All rights reserved.
//


import Foundation
import UIKit

class ServerResponse {
    
    var message: String
    var cipheredPrivateKey: String
    
    
    init?(message: String, cPK: String){
        self.message = message
        self.cipheredPrivateKey = cPK
    }
    
    
}
